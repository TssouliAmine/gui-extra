import React,{useState} from 'react'

function App() {

const[rgb,setRgb]= useState({red:0,green:0,blue:0});
const[hex,setHex]= useState('#000000');
const [converter, setConverter] = useState(1);
const [hexValue,setHexValue]= useState('000000');
const [rgbValue,setRgbValue] = useState({red:0,green:0,blue:0})

const checkHex = ()=>{ 
  hexValue.length !==6 ? alert('Only six-digit hex colors are allowed'):hexToRgb()
}
const checkRgb =()=> {
  let valid = true;
  if (rgbValue.red <0 || rgbValue.red>255) {
           valid = false; 
  }
  if (rgbValue.green <0 || rgbValue.green>255) {
    valid = false; 
}
if (rgbValue.blue <0 || rgbValue.blue>255) {
  valid = false; 
}
  valid?rgbToHex():alert('The values should be between 0 and 255')
}
const handleOnChange = (text,input) => {
  setRgbValue(prevState =>({...prevState,[input]:text}));
}

const hexToRgb = async () => {
  await fetch(`/api/hex-to-rgb?hex=${hexValue}`)
  .then(response => response.json())
  .catch(err=> setRgb(err))
  .then(json => setRgb(json))
};

const rgbToHex = async () => {
  await fetch(`/api/rgb-to-hex?red=${rgbValue.red}&green=${rgbValue.green}&blue=${rgbValue.blue}`)
  .then(response => response.text())
  .catch(err=> setHex(err))
  .then(text => setHex(text))
};


 if (converter) 
 return (
  <div style={{ display: 'flex',
   alignItems:'center',
  justifyContent: 'center',
  flexDirection:'column',
  height: '100vh',background:'pink'}}>
     

         <h3>HEX To RGB Converter</h3>
         <div style={{margin:20}} >

        <span>Hex color code (#RRGGBB)</span>
        </div>
        <span>#<input  type="text" value={hexValue} onChange={e => setHexValue(e.target.value)} /></span>
       
        <span style={{margin:18}}>Preview</span>
        <div style={{background:'#'+hexValue,width:300,height:80,border:'dashed',borderColor:'black',borderWidth:3,margin:20}}></div>
        <div >
          <span><button style={{width:100,color:'white',borderRadius:10,marginRight:10,padding:10,background:'#3285a8'}}onClick={checkHex}>Convert</button></span>
          <span><button style={{width:100,color:'white',borderRadius:10,padding:10,background:'#232467'}}onClick={()=>setConverter(!converter)}>Switch</button></span>
        </div>
        <div style={{margin:20,display:'flex',flexDirection:'column'}}>  
       <span style={{color:'red'}}>RED : {rgb.red} </span>
       <span style={{color:'green'}}>GREEN : {rgb.green} </span>
       <span style={{color:'blue'}}>BLUE : {rgb.blue}</span>
        </div>
 </div>
  )
  else 
  return (
    <div style={{ display: 'flex',
    alignItems:'center',
   justifyContent: 'center',
   flexDirection:'column',
   height: '100vh',background:'pink'}}>
      
 
          <h3>RGB To HEX Converter</h3>
          <div style={{display:'flex',flexDirection:'column',flexFlow:100}}>
          <span><label style={{marginLeft:20}}>RED (0-255) </label><input name='red' type="number" value={rgbValue.red} onChange={(e)=>handleOnChange(e.target.value,'red')} /></span>
        <span><label>GREEN (0-255) </label><input name='green' type="number" value={rgbValue.green} onChange={(e)=>handleOnChange(e.target.value,'green')} /></span>
        <span><label style={{marginLeft:13}}>BLUE (0-255) </label><input name='blue'  type="number" value={rgbValue.blue} onChange={(e)=>handleOnChange(e.target.value,'blue')} /></span>
        </div>
         <span style={{margin:18}}>Preview</span>
         <div style={{background:hex,width:300,height:80,border:'dashed',borderColor:'black',borderWidth:3,margin:20}}></div>
         <div >
           <span><button style={{width:100,color:'white',borderRadius:10,marginRight:10,padding:10,background:'#3285a8'}}onClick={checkRgb}>Convert</button></span>
           <span><button style={{width:100,color:'white',borderRadius:10,padding:10,background:'#232467'}}onClick={()=>setConverter(!converter)}>Switch</button></span>
         </div>
         <div style={{margin:20}}>  
        <span>{hex} </span>
         </div>
  </div>

  )
}

export default App